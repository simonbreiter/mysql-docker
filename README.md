# README

This repository is intended to be a minimal mysql configuration for docker.

## How to use

Build and start container:
```bash
./up
```

Shutdown container:
```bash
./down
```